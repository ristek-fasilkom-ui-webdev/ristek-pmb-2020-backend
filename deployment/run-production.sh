#!/bin/sh

docker pull "cahyanugraha12/pmb-backend:PRODUCTION"
docker-compose -f docker-compose.production.yml down
docker-compose -f docker-compose.production.yml up -d

# Removing unused images/images without at least one container associated
docker image prune -fa
