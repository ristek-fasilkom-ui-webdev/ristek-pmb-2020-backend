#!/bin/sh

# Some deployment files are unnecessary
rm -rf docker-compose.dev.yml docker-compose.production.yml docker-compose.staging.yml 
rm -rf migrate-production.sh migrate-staging.sh run-production.sh run-staging.sh

docker-compose -f docker-compose.yml down
docker-compose -f docker-compose.yml up -d

# Removing unused network/networks without at least one container associated
docker network prune -f