#!/bin/sh

docker-compose -f docker-compose.staging.yml run web-STAGING python manage.py makemigrations &&
docker-compose -f docker-compose.staging.yml run web-STAGING python manage.py migrate &&
docker-compose -f docker-compose.staging.yml run web-STAGING python manage.py collectstatic --noinput
