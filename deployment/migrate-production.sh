#!/bin/sh

docker-compose -f docker-compose.production.yml run web-PRODUCTION python manage.py makemigrations &&
docker-compose -f docker-compose.production.yml run web-PRODUCTION python manage.py migrate &&
docker-compose -f docker-compose.production.yml run web-PRODUCTION python manage.py collectstatic --noinput