#!/bin/sh

docker pull "cahyanugraha12/pmb-backend:STAGING"
docker-compose -f docker-compose.staging.yml down
docker-compose -f docker-compose.staging.yml up -d

# Removing unused images/images without at least one container associated
docker image prune -fa
