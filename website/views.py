import datetime
import random

from django.contrib.auth import get_user_model
from django.db import IntegrityError
from django.db.models import Max
from django.shortcuts import get_object_or_404
from rest_framework import generics, permissions, status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from account.permissions import IsMaba

from .models import (Announcement, Event, Puzzle, PuzzleQuestion, PuzzleStatus,
                     PuzzleTask, PuzzleUserStatistics, Question, Submission,
                     Task)
from .payloads import SubmitPuzzleSerializer
from .permissions import IsPuzzleOwner
from .serializers import (AnnouncementSerializer, CreateSubmissionSerializer,
                          EventSerializer, PuzzleSerializer,
                          PuzzleTaskSerializer, PuzzleUserStatisticsSerializer,
                          SubmissionSerializer, TaskDetailSerializer,
                          TaskSerializer, UploadFileSerializer)


class EventList(generics.ListAPIView):
    """
    API View for Event collection. Define GET method.
    """
    queryset = Event.objects.all()
    serializer_class = EventSerializer

class EventDetail(generics.RetrieveAPIView):
    """
    API View for Event member. Define GET method.
    """
    queryset = Event.objects.all()
    serializer_class = EventSerializer

class PuzzleAPIView(APIView):
    """
    API View for Puzzle collection. Define GET method.
    """
    permission_classes = [IsMaba]

    def get(self, request):
        current_user = request.user

        maba_same_big_group_users = get_user_model().objects.filter(
            profile__maba__group__big_group=current_user.profile.maba.group.big_group,
        )

        puzzle_target_maba_user_ids = Puzzle.objects.filter(
            puzzle_creator=current_user,
        ).values_list('target_maba__id', flat=True)

        # Checking if there are Puzzle that have not been created for some same big group maba.
        for user in maba_same_big_group_users:
            if user.id != current_user.id and user.id not in puzzle_target_maba_user_ids:
                puzzle_obj = Puzzle.objects.create(
                    puzzle_creator=current_user,
                    target_maba=user,
                )

                selected_question_ids = []
                max_question_id = Question.objects.aggregate(
                    max_question_id=Max('id'),
                )['max_question_id']
                while(len(selected_question_ids) < 5 and Question.objects.count() >= 5):
                    random_id = random.randint(1, max_question_id)
                    if random_id not in selected_question_ids:
                        try:
                            question_obj = Question.objects.get(id=random_id)
                            PuzzleQuestion.objects.create(puzzle=puzzle_obj, question=question_obj)

                            selected_question_ids.append(random_id)
                        except Question.DoesNotExist:
                            continue

        puzzles = Puzzle.objects.filter(puzzle_creator=current_user)
        serialized_puzzles = PuzzleSerializer(puzzles, many=True)
        return Response(serialized_puzzles.data, status=status.HTTP_200_OK)

class PuzzleDetailAPIView(generics.RetrieveAPIView):
    """
    API View for Puzzle member. Define GET and PATCH method.
    """
    permission_classes = [IsMaba, IsPuzzleOwner]
    queryset = Puzzle.objects.all()
    serializer_class = PuzzleSerializer

    def patch(self, request, pk):
        puzzle_obj = Puzzle.objects.get(id=pk)
        self.check_object_permissions(request, puzzle_obj)

        valid_status_for_submit_puzzle = [PuzzleStatus.EMPTY.name, PuzzleStatus.REJECTED.name]
        if puzzle_obj.status not in valid_status_for_submit_puzzle:
            return Response(
                data={'message': 'Current puzzle status does not allow changes.'},
                status=status.HTTP_400_BAD_REQUEST,
            )

        deserialized_submit_puzzle = SubmitPuzzleSerializer(puzzle_obj, data=request.data)
        if deserialized_submit_puzzle.is_valid():
            try:
                next_status = PuzzleStatus.VERIFYING.name
                if puzzle_obj.status == PuzzleStatus.REJECTED.name:
                    next_status = PuzzleStatus.RESUBMITTING.name

                puzzle_obj = deserialized_submit_puzzle.save(status=next_status)
                serialized_puzzle = PuzzleSerializer(puzzle_obj)
                return Response(serialized_puzzle.data, status=status.HTTP_200_OK)
            except IntegrityError:
                return Response(
                    data={'message': 'Integrity Error: One of Questions and Answers ID is probably invalid.'},
                    status=status.HTTP_400_BAD_REQUEST,
                )
        return Response(deserialized_submit_puzzle.errors, status=status.HTTP_400_BAD_REQUEST)

class PuzzleTaskList(generics.ListAPIView):
    queryset = PuzzleTask.objects.all()
    serializer_class = PuzzleTaskSerializer
    permission_classes = [permissions.IsAuthenticated]

class PuzzleUserStatisticsList(generics.ListAPIView):
    serializer_class = PuzzleUserStatisticsSerializer
    permission_classes = [IsMaba]

    def get_queryset(self):
        return PuzzleUserStatistics.objects.filter(user=self.request.user)

class TaskList(APIView):
    """
    API View for Task collection. Define GET method.
    """
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        user = request.user
        submissions = Submission.objects.filter(user=user)
        submissions_task_ids = {s.task.id for s in submissions}

        tasks = Task.objects.all()

        serializer = TaskSerializer(
            tasks,
            many=True,
            context={'submissions_task_ids': submissions_task_ids}
        )

        return Response(serializer.data, status=status.HTTP_200_OK)

class TaskDetail(APIView):
    """
    API View for Task member. Define GET method.
    """
    permission_classes = (IsAuthenticated,)

    def get(self, request, pk):
        user = request.user

        task = get_object_or_404(Task, id=pk)

        serializer = TaskDetailSerializer(
            task,
            context={
                'is_submission_exist': Submission.objects.filter(user=user, task=task).exists()
            }
        )

        return Response(serializer.data, status=status.HTTP_200_OK)

class TaskSubmissionList(APIView):
    """
    API View for Submission. Define GET and POST method.
    """

    def get_task(self, pk_task):
        return get_object_or_404(Task, id=pk_task)

    def get(self, request, pk_task):
        task = self.get_task(pk_task)
        submissions = Submission.objects.filter(task=task, user=request.user)
        serialized_submissions = SubmissionSerializer(submissions, many=True)

        return Response(serialized_submissions.data, status=status.HTTP_200_OK)

    def post(self, request, pk_task):
        task = self.get_task(pk_task)

        data = request.data

        data['user'] = self.request.user.id
        data['task'] = task.id

        serialized_submission = CreateSubmissionSerializer(data=data)
        if serialized_submission.is_valid():
            created_submission = serialized_submission.save()
            serialized_created_submission = SubmissionSerializer(created_submission)
            return Response(serialized_created_submission.data, status=status.HTTP_201_CREATED)
        return Response(serialized_submission.errors, status=status.HTTP_400_BAD_REQUEST)

class UploadFileAPIView(APIView):
    """
    API View for upload file. Define POST method.
    """
    permission_classes = (IsAuthenticated,)
    parser_classes = (MultiPartParser, FormParser)

    def post(self, request):
        serializer = UploadFileSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class AnnouncementAPIView(generics.ListAPIView):
    """
    API View for Announcement collection. Define GET method.
    """
    queryset = Announcement.objects.all()
    serializer_class = AnnouncementSerializer

@api_view(['GET'])
@permission_classes([permissions.AllowAny])
def get_server_time(request):
    now = datetime.datetime.now()
    return Response({'server_time': now})
