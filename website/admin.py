from django.contrib import admin
from django.utils.html import format_html

from backend.admin import ExportCsvMixin
from website.models import (Announcement, Event, Puzzle, PuzzleQuestion, PuzzleTask,
                            PuzzleUserStatistics, Question, Submission, Task, UploadFile)


class EventModelAdmin(admin.ModelAdmin):
    list_display = ['title', 'date', 'place', 'organizer']
    search_fields = ['title', 'place', 'organizer']
    list_filter = ['organizer']

    class Meta:
        model = Event

class QuestionModelAdmin(admin.ModelAdmin):
    list_display = ['question']
    search_fields = ['question']

    class Meta:
        model = Question

class PuzzleQuestionModelAdmin(admin.ModelAdmin):
    class Meta:
        model = PuzzleQuestion

class PuzzleQuestionInline(admin.TabularInline):
    model = PuzzleQuestion
    max_num = 5
    can_delete = False

    def has_add_permission(self, request, obj=None):
        return False

    def has_edit_permission(self, request, obj=None):
        return False

class PuzzleModelAdmin(admin.ModelAdmin):
    list_display = [
        'profile_puzzle_creator',
        'puzzle_creator_code',
        'profile_target_maba',
        'attachment_link',
        'status'
    ]
    search_fields = [
        'puzzle_creator__profile__name',
        'puzzle_creator__profile__maba__code'
    ]
    list_filter = ['status', 'puzzle_creator__profile__maba__group__big_group']
    inlines = [PuzzleQuestionInline,]

    def profile_puzzle_creator(self, obj):
        return obj.puzzle_creator.profile

    def profile_target_maba(self, obj):
        return obj.target_maba.profile

    def puzzle_creator_code(self, obj):
        return obj.puzzle_creator.profile.maba.code

    class Meta:
        model = Puzzle

class PuzzleTaskModelAdmin(admin.ModelAdmin):
    list_display = ['title', 'start_date', 'end_date', 'required_puzzle']
    search_fields = ['title']

    class Meta:
        model = PuzzleTask

class PuzzleUserStatisticModelAdmin(admin.ModelAdmin, ExportCsvMixin):
    list_display = ['user_name',
                    'puzzle_task_title',
                    'puzzle_submitted',
                    'puzzle_approved'
                    ]
    search_fields = ['user__profile__name', 'puzzle_task__title']
    list_filter = ['puzzle_task__title']
    actions = ["export_as_csv"]

    def user_name(self, obj):
        return obj.user.profile.name

    def puzzle_task_title(self, obj):
        return obj.puzzle_task.title

    user_name.admin_order_field = 'user__profile__name'
    puzzle_task_title.admin_order_field = 'puzzle_task__title'

    class Meta:
        model = PuzzleUserStatistics

class TaskModelAdmin(admin.ModelAdmin):
    list_display = ['title', 'start_time', 'end_time', 'description']
    search_fields = ['title']

    class Meta:
        model = Task

class AnnouncementModelAdmin(admin.ModelAdmin):
    list_display = ['title', 'date', 'description']
    search_fields = ['title']

    class Meta:
        model = Announcement

class SubmissionModelAdmin(admin.ModelAdmin):
    list_display = ['profile', 'task', 'file_link_clickable', 'submit_time', 'created_at', 'evaluator', 'status']
    list_display_links = ['profile', 'task']
    search_fields = ['user__profile__name', 'task__title']
    list_filter = ['task', 'user__profile__maba__evaluator', 'status']

    def profile(self, obj):
        return obj.user.profile

    def evaluator(self, obj):
        return obj.user.profile.maba.evaluator

    def file_link_clickable(self, obj):
        return format_html('<a href="{}">{}</a>', obj.file_link, obj.file_link)

    profile.admin_order_field = 'user__profile__name'
    evaluator.admin_order_field = 'user__profile__maba__evaluator'
    file_link_clickable.short_description = 'File Link'

    class Meta:
        model = Submission

admin.site.register(Event, EventModelAdmin)
admin.site.register(Question, QuestionModelAdmin)
admin.site.register(PuzzleQuestion, PuzzleQuestionModelAdmin)
admin.site.register(Puzzle, PuzzleModelAdmin)
admin.site.register(PuzzleTask, PuzzleTaskModelAdmin)
admin.site.register(PuzzleUserStatistics, PuzzleUserStatisticModelAdmin)
admin.site.register(Task, TaskModelAdmin)
admin.site.register(Announcement, AnnouncementModelAdmin)
admin.site.register(Submission, SubmissionModelAdmin)
admin.site.register(UploadFile)
