from drf_writable_nested.mixins import NestedUpdateMixin
from rest_framework import serializers

from .models import Puzzle, PuzzleQuestion


class SubmitPuzzleQuestionSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()

    class Meta:
        model = PuzzleQuestion
        fields = ['id', 'answer']


class SubmitPuzzleSerializer(NestedUpdateMixin, serializers.ModelSerializer):
    questions_and_answers = SubmitPuzzleQuestionSerializer(many=True)

    class Meta:
        model = Puzzle
        fields = ['id', 'attachment_link', 'questions_and_answers']

    def validate_questions_and_answers(self, value):
        instance_questions_and_answers_ids = self.instance.questions_and_answers.all().values_list(
            'id',
            flat=True,
        )
        value_questions_and_answers_ids = [val['id'] for val in value]

        # Make sure there is no duplicate ids
        if len(value_questions_and_answers_ids) != len(set(value_questions_and_answers_ids)):
            raise serializers.ValidationError("Duplicate questions and answers id not allowed!")

        # Make sure all instance of questions and answer is updated (we do not allow partial update)
        for qna_id in instance_questions_and_answers_ids:
            if qna_id not in value_questions_and_answers_ids:
                raise serializers.ValidationError("All questions must be answered!")

        return value

    def update(self, instance, validated_data):
        relations, reverse_relations = self._extract_relations(validated_data)

        # Create or update direct relations (foreign key, one-to-one)
        self.update_or_create_direct_relations(
            validated_data,
            relations,
        )

        # Update instance
        instance = super(SubmitPuzzleSerializer, self).update(
            instance,
            validated_data,
        )
        self.update_or_create_reverse_relations(instance, reverse_relations)
        return instance
