import os
import magic

from django.conf import settings
from django.template.defaultfilters import filesizeformat
from rest_framework import serializers

from account.serializers import MabaSerializer, UserSerializer
from website.models import (Announcement, Event, Puzzle, PuzzleQuestion,
                            PuzzleTask, PuzzleUserStatistics, Question,
                            Submission, Task, UploadFile)


class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = '__all__'

class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = '__all__'

class PuzzleQuestionSerializer(serializers.ModelSerializer):
    question = QuestionSerializer()

    class Meta:
        model = PuzzleQuestion
        fields = '__all__'

class PuzzleSerializer(serializers.ModelSerializer):
    target_maba = serializers.SerializerMethodField()
    questions_and_answers = serializers.SerializerMethodField()

    class Meta:
        model = Puzzle
        fields = ['id', 'puzzle_creator', 'target_maba', 'attachment_link', 'questions_and_answers', 'status']

    def get_target_maba(self, obj):
        target_maba = obj.target_maba.profile
        serialized_maba = MabaSerializer(target_maba.maba)

        return serialized_maba.data

    def get_questions_and_answers(self, obj):
        questions_and_answers = obj.questions_and_answers
        serialized_questions_and_answers = PuzzleQuestionSerializer(
            questions_and_answers,
            many=True,
        )

        return serialized_questions_and_answers.data

class PuzzleTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = PuzzleTask
        fields = '__all__'

class PuzzleUserStatisticsSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    puzzle_task = PuzzleTaskSerializer()

    class Meta:
        model = PuzzleUserStatistics
        fields = '__all__'

class TaskSerializer(serializers.ModelSerializer):
    is_submitted = serializers.SerializerMethodField()

    class Meta:
        model = Task
        fields = '__all__'

    def get_is_submitted(self, obj):
        submissions_task_ids = self.context.get('submissions_task_ids')

        if submissions_task_ids is None:
            return False

        if obj.can_have_submission and obj.id in submissions_task_ids:
            return True

        return False

class TaskDetailSerializer(TaskSerializer):
    def get_is_submitted(self, obj):
        return obj.can_have_submission and self.context.get('is_submission_exist')

class SubmissionSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    task = TaskSerializer()

    class Meta:
        model = Submission
        fields = '__all__'

class CreateSubmissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Submission
        fields = ['user', 'task', 'file_link', 'submit_time']

class UploadFileSerializer(serializers.ModelSerializer):
    def validate_file(self, value):
        content_types = [
            'application/msword',
            'application/CDFV2',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/pdf',
            'application/zip',
            'application/vnd.rar'
        ]
        max_upload_size = 3145728

        if max_upload_size and value.size > max_upload_size:
            # Ensure no one bypasses the js checker
            raise serializers.ValidationError(
                "File size exceeds limit: %(current_size)s. Limit is %(max_size)s."
                % {
                    "max_size": filesizeformat(max_upload_size),
                    "current_size": filesizeformat(value.size),
                }
            )

        file = value.file
        uploaded_content_type = getattr(file, "content_type", "")

        # magic_file_path used only for Windows.
        magic_file_path = getattr(settings, "MAGIC_FILE_PATH", None)
        if magic_file_path and os.name == "nt":
            mg = magic.Magic(mime=True, magic_file=magic_file_path)
        else:
            mg = magic.Magic(mime=True)
        content_type_magic = mg.from_buffer(file.read(4096))
        file.seek(0)

        # Prefer mime-type from magic over mime-type from http header
        if uploaded_content_type != content_type_magic:
            uploaded_content_type = content_type_magic

        if uploaded_content_type not in content_types:
            raise serializers.ValidationError(
                "Unsupported file type: %(type)s. Allowed types are %(allowed)s."
                % {"type": content_type_magic, "allowed": content_types}
            )

        return value

    class Meta:
        model = UploadFile
        fields = '__all__'

class AnnouncementSerializer(serializers.ModelSerializer):
    class Meta:
        model = Announcement
        fields = '__all__'
