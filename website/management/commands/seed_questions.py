import json
import os

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from website.models import Question


class Command(BaseCommand):
    help = 'Seed questions.'

    def handle(self, *args, **options):
        try:
            base_dir = settings.BASE_DIR
            questions_file_path = os.path.join(base_dir, 'static', 'data', 'questions.json')
            with open(questions_file_path) as questions_file:
                questions_json = json.load(questions_file)
                questions_obj = []
                for question in questions_json:
                    questions_obj.append(Question(question=question))
                Question.objects.bulk_create(questions_obj)
        except FileNotFoundError:
            raise CommandError(
                'This command require an json file at ' +
                questions_file_path +
                ' filled with an array of strings'
            )
