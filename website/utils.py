from website.models import Puzzle, PuzzleStatus


def is_same_big_group(user, other_user):
    """
    Determine if a user is in same big group with the other user.
    """
    profile_user = user.profile
    profile_other_user = other_user.profile

    big_group_user = profile_user.maba.group.big_group
    big_group_other_user = profile_other_user.maba.group.big_group

    return big_group_user == big_group_other_user

def update_puzzle_statistic_for_puzzle_creator(puzzle_statistic, puzzle_creator):
    puzzle_statistic.puzzle_submitted = Puzzle.objects.filter(
        puzzle_creator=puzzle_creator
    ).exclude(status=PuzzleStatus.EMPTY.name).count()

    puzzle_statistic.puzzle_approved = Puzzle.objects.filter(
        puzzle_creator=puzzle_creator,
        status=PuzzleStatus.VERIFIED.name
    ).count()

    puzzle_statistic.save()
