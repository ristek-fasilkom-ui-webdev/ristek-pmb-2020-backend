from rest_framework import permissions


class IsPuzzleOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        try:
            return request.user == obj.puzzle_creator
        except Exception as error:
            raise error
