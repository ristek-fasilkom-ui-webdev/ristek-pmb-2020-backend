from enum import Enum

from ckeditor.fields import RichTextField
from constrainedfilefield.fields import ConstrainedFileField
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.db import models


class Event(models.Model):
    title = models.CharField(max_length=255)
    date = models.DateTimeField(auto_now=False, auto_now_add=False)
    place = models.CharField(max_length=255)
    description = RichTextField(blank=True)
    attachment_link = models.URLField(
        blank=True,
        help_text='Must start with "https://" or "http://", eg. "https://google.com"',
    )
    cover_image_link = models.URLField(
        blank=True,
        help_text='Must start with "https://" or "http://", eg. "https://google.com"',
    )
    organizer = models.CharField(max_length=255, blank=True)
    organizer_image_link = models.URLField(
        blank=True,
        help_text='Must start with "https://" or "http://", eg. "https://google.com"',
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-date']
        verbose_name = 'Event'
        verbose_name_plural = 'Events'

    def __str__(self):
        return self.title

class Question(models.Model):
    question = models.CharField(max_length=255, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['question']
        verbose_name = 'Question'
        verbose_name_plural = 'Questions'

    def __str__(self):
        return self.question

class PuzzleStatus(Enum):
    EMPTY = 'Empty'
    VERIFYING = 'Waiting for verification'
    REJECTED = 'Rejected'
    RESUBMITTING = 'Resubmit'
    VERIFIED = 'Verified'

    @staticmethod
    def get_default():
        return PuzzleStatus.EMPTY.name

class Puzzle(models.Model):
    puzzle_creator = models.ForeignKey(
        get_user_model(),
        related_name='created_puzzle',
        on_delete=models.CASCADE,
        db_index=True,
    )
    target_maba = models.ForeignKey(
        get_user_model(),
        related_name='targeted_puzzle',
        on_delete=models.CASCADE,
        db_index=True,
    )
    attachment_link = models.URLField(
        blank=True,
        help_text='Must start with "https://" or "http://", eg. "https://google.com"',
    )
    status = models.CharField(
        max_length=30,
        choices=[(status.name, status.value) for status in PuzzleStatus],
        default=PuzzleStatus.get_default(),
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['puzzle_creator', 'target_maba'],
                name='unique_puzzle_creator_target',
            ),
        ]
        ordering = ['puzzle_creator__profile__name', 'target_maba__profile__name']
        verbose_name = 'Puzzle'
        verbose_name_plural = 'Puzzles'

    def __str__(self):
        return 'Puzzle created by ' \
            + self.puzzle_creator.profile.name \
            + ' with target ' \
            + self.target_maba.profile.name

class PuzzleQuestion(models.Model):
    puzzle = models.ForeignKey(
        Puzzle,
        on_delete=models.CASCADE,
        related_name='questions_and_answers',
    )
    question = models.ForeignKey(
        Question,
        on_delete=models.CASCADE,
        related_name='puzzles_and_answers',
    )
    answer = models.TextField(blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['puzzle', 'question'],
                name='unique_puzzle_question_pair'
            ),
        ]
        ordering = ['puzzle__puzzle_creator__profile__name', 'question__question']
        verbose_name = 'Puzzle Question'
        verbose_name_plural = 'Puzzles Questions'

    def __str__(self):
        return 'Puzzle Question for ' + str(self.puzzle) + " | " + str(self.question)

class PuzzleTask(models.Model):
    title = models.CharField(max_length=255)
    start_date = models.DateTimeField(auto_now=False, auto_now_add=False)
    end_date = models.DateTimeField(auto_now=False, auto_now_add=False)

    required_puzzle = models.PositiveIntegerField()

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['end_date', 'start_date']
        verbose_name = 'Puzzle Task'
        verbose_name_plural = 'Puzzle Tasks'

    def __str__(self):
        return self.title

class PuzzleUserStatistics(models.Model):
    user = models.ForeignKey(
        get_user_model(),
        related_name="puzzle_statistics",
        on_delete=models.CASCADE
    )
    puzzle_task = models.ForeignKey(
        PuzzleTask,
        related_name="puzzle_statistics",
        on_delete=models.CASCADE
    )

    puzzle_submitted = models.PositiveIntegerField(default=0)
    puzzle_approved = models.PositiveIntegerField(default=0)

    class Meta:
        ordering = ['puzzle_task__end_date', 'puzzle_task__start_date']
        verbose_name = 'Puzzle User Statistics'
        verbose_name_plural = 'Puzzle User Statistics'

    def __str__(self):
        return "Puzzle Statistic by " + \
            self.user.profile.name + \
            " for task " + \
            self.puzzle_task.title

class Task(models.Model):
    title = models.CharField(max_length=255)
    start_time = models.DateTimeField(auto_now=False, auto_now_add=False)
    end_time = models.DateTimeField(auto_now=False, auto_now_add=False)
    description = RichTextField(blank=True)
    attachment_link = models.URLField(
        blank=True,
        help_text='Must start with "https://" or "http://", eg. "https://google.com"',
    )
    can_have_submission = models.BooleanField(default=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['end_time', 'start_time']
        verbose_name = 'Task'
        verbose_name_plural = 'Tasks'

    def __str__(self):
        return self.title

class UploadFile(models.Model):
    # Max Upload File = 3MB
    file = ConstrainedFileField(
        content_types=[
            'application/msword',
            'application/CDFV2',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/pdf',
            'application/zip',
            'application/vnd.rar'
        ],
        max_upload_size=3145728
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['created_at']

    def __str__(self):
        return str(self.file)

class SubmissionStatus(Enum):
    PENDING = 'Pending'
    ACCEPTED = 'Accepted'
    REJECTED = 'Rejected'

    @staticmethod
    def get_default():
        return SubmissionStatus.PENDING.name

class Submission(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    file_link = models.URLField(
        blank=True,
        help_text='Must start with "https://" or "http://", eg. "https://google.com"',
    )
    submit_time = models.DateTimeField(auto_now=False, auto_now_add=False, null=True)
    status = models.CharField(
        max_length=10,
        choices=[(status.name, status.value) for status in SubmissionStatus],
        default=SubmissionStatus.get_default()
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-submit_time', '-created_at']
        verbose_name = 'Submission'
        verbose_name_plural = 'Submissions'

    def __str__(self):
        return "Submission by " + self.user.username + " for task " + self.task.title

class Announcement(models.Model):
    title = models.CharField(max_length=255)
    date = models.DateTimeField(auto_now=False, auto_now_add=False)
    description = RichTextField(blank=True)
    attachment_link = models.URLField(
        blank=True,
        help_text='Must start with "https://" or "http://", eg. "https://google.com"',
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-date']
        verbose_name = 'Announcement'
        verbose_name_plural = 'Announcements'

    def __str__(self):
        return self.title
