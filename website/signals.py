from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone

from account.models import Maba
from website.models import (Puzzle, PuzzleTask, PuzzleUserStatistics)
from website.utils import update_puzzle_statistic_for_puzzle_creator


@receiver(post_save, sender=Maba)
def on_maba_create_or_update_create_puzzle_user_statistics(sender, created, instance, **kwargs):
    if created:
        puzzle_tasks = PuzzleTask.objects.all()
        user = instance.profile.user
        for task in puzzle_tasks:
            PuzzleUserStatistics.objects.create(user=user, puzzle_task=task)

@receiver(post_save, sender=PuzzleTask)
def create_puzzle_user_statistics_when_puzzle_task_created(sender, created, instance, **kwargs):
    if created:
        puzzle_task = instance
        maba_list = Maba.objects.all()

        for maba in maba_list:
            user = maba.profile.user
            user_statistic = PuzzleUserStatistics.objects.create(
                user=user,
                puzzle_task=puzzle_task
            )
            update_puzzle_statistic_for_puzzle_creator(user_statistic, user)

@receiver(post_save, sender=Puzzle)
def update_puzzle_user_statistics_when_puzzle_created_or_updated(sender, created, instance, **kwargs):
    puzzle_creator = instance.puzzle_creator
    user_statistics = PuzzleUserStatistics.objects.filter(user=puzzle_creator)
    for user_statistic in user_statistics:
        puzzle_task = user_statistic.puzzle_task
        now = timezone.now()
        if puzzle_task.end_date >= now and puzzle_task.start_date <= now:
            update_puzzle_statistic_for_puzzle_creator(user_statistic, puzzle_creator)
