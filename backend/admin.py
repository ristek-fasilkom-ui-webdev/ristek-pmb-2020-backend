import csv
import os

from django.contrib.admin import AdminSite
from django.contrib.auth.views import LogoutView
from django.http import HttpResponse
from django.shortcuts import redirect, reverse

from account.models import UserProfile


class ExportCsvMixin:
    def get_object_attr(self, obj, field, meta):
        """
        Special rules for some model. Not scalable, find better solution if time allows.
        """
        if str(meta) == 'friends.kenalanuserstatistics' and field == 'name':
            return obj.user.profile.name
        elif str(meta) == 'website.puzzleuserstatistics' and field == 'name':
            return obj.user.profile.name
        return getattr(obj, field)

    def export_as_csv(self, request, queryset):
        meta = self.model._meta
        field_names = [field.name for field in meta.fields]

        """
        Special rules for some model. Not scalable, find better solution if time allows.
        """
        if str(meta) == 'friends.kenalanuserstatistics':
            field_names.insert(2, 'name')
        elif str(meta) == 'website.puzzleuserstatistics':
            field_names.insert(2, 'name')

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(meta)
        writer = csv.writer(response)

        writer.writerow(field_names)
        for obj in queryset:
            writer.writerow([self.get_object_attr(obj, field, meta) for field in field_names])

        return response

    export_as_csv.short_description = "Export selected as CSV"

class PMBBackendAdminSite(AdminSite):
    site_title = 'PMB Site Admin'
    site_header = 'PMB Administration'
    index_title = 'PMB Site Administration'

    login_template = 'backend/admin_login.html'

    def logout(self, request, extra_context=None):
        """
        This section works on the assumption
        that a SSO Account have a Profile associated with it
        """
        if request.user.is_authenticated:
            user = request.user
            if UserProfile.objects.filter(user=user).exists():
                print(request)
                print(request.META)
                admin_url = request.META['wsgi.url_scheme'] + '://' + request.META['HTTP_HOST'] + reverse('admin:index')
                cas_logout_url = reverse('cas_ng_logout')
                return redirect(cas_logout_url + '?next=' + admin_url)

        """
        Original log out function from AdminSite

        Log out the user for the given HttpRequest.

        This should *not* assume the user is already logged in.
        """
        defaults = {
            'extra_context': {
                **self.each_context(request),
                # Since the user isn't logged out at this point, the value of
                # has_permission must be overridden.
                'has_permission': False,
                **(extra_context or {})
            },
        }
        if self.logout_template is not None:
            defaults['template_name'] = self.logout_template
        request.current_app = self.name
        return LogoutView.as_view(**defaults)(request)
