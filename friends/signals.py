from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone

from account.models import Maba
from friends.models import (Kenalan, KenalanTask,
                            KenalanUserStatistics)
from friends.utils import update_kenalan_statistic_for_user_maba


@receiver(post_save, sender=Maba)
def on_maba_create_or_update_create_kenalan_user_statistics(sender, created, instance, **kwargs):
    if created:
        kenalan_tasks = KenalanTask.objects.all()
        user = instance.profile.user
        for task in kenalan_tasks:
            KenalanUserStatistics.objects.create(user=user, kenalan_task=task)

@receiver(post_save, sender=KenalanTask)
def create_kenalan_user_statistics_when_kenalan_task_created(sender, created, instance, **kwargs):
    if created:
        kenalan_task = instance
        maba_list = Maba.objects.all()

        for maba in maba_list:
            user = maba.profile.user
            user_statistic = KenalanUserStatistics.objects.create(
                user=user,
                kenalan_task=kenalan_task
            )
            update_kenalan_statistic_for_user_maba(user_statistic, user)

@receiver(post_save, sender=Kenalan)
def update_kenalan_user_statistics_when_kenalan_created_or_updated(sender, created, instance, **kwargs):
    user_maba = instance.user_maba
    user_statistics = KenalanUserStatistics.objects.filter(user=user_maba)
    for user_statistic in user_statistics:
        kenalan_task = user_statistic.kenalan_task
        now = timezone.now()
        if kenalan_task.end_date >= now and kenalan_task.start_date <= now:
            update_kenalan_statistic_for_user_maba(user_statistic, user_maba)
