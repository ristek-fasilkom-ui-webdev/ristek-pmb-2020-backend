from django.contrib import admin

from backend.admin import ExportCsvMixin
from friends.models import Token, Kenalan, KenalanStatus, KenalanTask, KenalanUserStatistics


class KenalanModelAdmin(admin.ModelAdmin):
    list_display = ['profile_maba', 'maba_code', 'profile_elemen', 'description', 'status']
    search_fields = [
        'user_maba__profile__name',
        'user_maba__profile__maba__code'
    ]
    list_filter = ['status', 'user_maba__profile__maba__group__big_group']

    def profile_maba(self, obj):
        return obj.user_maba.profile

    def profile_elemen(self, obj):
        return obj.user_elemen.profile

    def maba_code(self, obj):
        return obj.user_maba.profile.maba.code

    def get_queryset(self, request):
        if request.user.is_superuser:
            return Kenalan.objects.all()
        return Kenalan.objects.all().exclude(status=KenalanStatus.NOT_FRIEND.name)

    profile_maba.admin_order_field = 'user_maba__profile__name'
    profile_elemen.admin_order_field = 'user_elemen__profile__name'

    class Meta:
        model = Kenalan

class KenalanTaskModelAdmin(admin.ModelAdmin):
    list_display = ['title', 'start_date', 'end_date', 'required_maung', 'required_quanta', 'required_tarung', 'required_elemen', 'required_all']
    search_fields = ['title']

    class Meta:
        model = KenalanTask

class KenalanUserStatisticModelAdmin(admin.ModelAdmin, ExportCsvMixin):
    list_display = ['user_name',
                    'kenalan_task_title',
                    'kenalan_created_maung',
                    'kenalan_created_quanta',
                    'kenalan_created_tarung',
                    'kenalan_created_elemen',
                    'kenalan_created_all',
                    'kenalan_approved_maung',
                    'kenalan_approved_quanta',
                    'kenalan_approved_tarung',
                    'kenalan_approved_elemen',
                    'kenalan_approved_all'
                    ]
    search_fields = ['user__profile__name', 'kenalan_task__title']
    list_filter = ['kenalan_task__title']
    actions = ["export_as_csv"]

    def user_name(self, obj):
        return obj.user.profile.name

    def kenalan_task_title(self, obj):
        return obj.kenalan_task.title

    user_name.admin_order_field = 'user__profile__name'
    kenalan_task_title.admin_order_field = 'kenalan_task__title'

    class Meta:
        model = KenalanUserStatistics

admin.site.register(Token)
admin.site.register(Kenalan, KenalanModelAdmin)
admin.site.register(KenalanTask, KenalanTaskModelAdmin)
admin.site.register(KenalanUserStatistics, KenalanUserStatisticModelAdmin)
