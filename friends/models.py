from datetime import timedelta
from enum import Enum

from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone


def default_token_end_time():
    now = timezone.now()
    end_time = now + timedelta(minutes=5)
    return end_time

class Token(models.Model):
    token = models.CharField(max_length=6, primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    count = models.SmallIntegerField(default=1)
    start_time = models.DateTimeField(auto_now=True)
    end_time = models.DateTimeField(default=default_token_end_time)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['token']

    def __str__(self):
        return "Token number " + self.token + " by " + self.user.username

class KenalanStatus(Enum):
    NOT_FRIEND = 'Not Friend'
    PENDING = 'Pending'
    APPROVED = 'Approved'
    REJECTED = 'Rejected'
    SAVED = 'Saved'

    @staticmethod
    def get_default():
        return KenalanStatus.NOT_FRIEND.name

class Kenalan(models.Model):
    user_maba = models.ForeignKey(
        User,
        related_name="user_maba",
        on_delete=models.CASCADE
    )
    user_elemen = models.ForeignKey(
        User,
        related_name='user_elemen',
        on_delete=models.CASCADE
    )
    description = models.TextField(blank=True)
    rejection_message = models.TextField(blank=True)
    status = models.CharField(
        max_length=10,
        choices=[(status.name, status.value) for status in KenalanStatus],
        default=KenalanStatus.get_default()
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-created_at', '-updated_at']
        constraints = [
            models.UniqueConstraint(
                fields=['user_maba', 'user_elemen'],
                name='unique_kenalan'
            )
        ]
        verbose_name = 'Kenalan'
        verbose_name_plural = 'Kenalan'

    def __str__(self):
        return "Kenalan by " + self.user_maba.username + " with " + self.user_elemen.username

class KenalanTask(models.Model):
    title = models.CharField(max_length=255)
    start_date = models.DateTimeField(auto_now=False, auto_now_add=False)
    end_date = models.DateTimeField(auto_now=False, auto_now_add=False)

    """
    Batch specific fields, change it as necessary.
    """
    required_maung = models.PositiveIntegerField()
    required_quanta = models.PositiveIntegerField()
    required_tarung = models.PositiveIntegerField()
    required_elemen = models.PositiveIntegerField()
    required_all = models.PositiveIntegerField()

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['end_date', 'start_date']
        verbose_name = 'Kenalan Task'
        verbose_name_plural = 'Kenalan Tasks'

    def __str__(self):
        return self.title

class KenalanUserStatistics(models.Model):
    user = models.ForeignKey(User, related_name="kenalan_statistics", on_delete=models.CASCADE)
    kenalan_task = models.ForeignKey(
        KenalanTask,
        related_name="kenalan_statistics",
        on_delete=models.CASCADE
    )

    """
    Batch specific fields, change it as necessary.
    """
    kenalan_created_maung = models.PositiveIntegerField(default=0)
    kenalan_created_quanta = models.PositiveIntegerField(default=0)
    kenalan_created_tarung = models.PositiveIntegerField(default=0)
    kenalan_created_elemen = models.PositiveIntegerField(default=0)
    kenalan_created_all = models.PositiveIntegerField(default=0)

    kenalan_approved_maung = models.PositiveIntegerField(default=0)
    kenalan_approved_quanta = models.PositiveIntegerField(default=0)
    kenalan_approved_tarung = models.PositiveIntegerField(default=0)
    kenalan_approved_elemen = models.PositiveIntegerField(default=0)
    kenalan_approved_all = models.PositiveIntegerField(default=0)

    class Meta:
        ordering = ['kenalan_task__end_date', 'kenalan_task__start_date']
        verbose_name = 'Kenalan User Statistics'
        verbose_name_plural = 'Kenalan User Statistics'

    def __str__(self):
        return "Kenalan Statistic by " + \
            self.user.profile.name + \
            " for task " + \
            self.kenalan_task.title
