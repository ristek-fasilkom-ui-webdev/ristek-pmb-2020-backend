from django.utils import timezone

from account.utils import BATCH_NAME, BATCH_NAME_NON_ACTIVE

from friends.models import Kenalan, KenalanStatus, Token

def delete_expired_token(user):
    now = timezone.now()

    expired_token = Token.objects.filter(user=user, end_time__lt=now)
    if expired_token.exists():
        expired_token.delete()

def update_kenalan_statistic_for_user_maba(user_statistic, user_maba):
    """
    Batch specific fields. Change as necessary
    """
    user_statistic.kenalan_created_maung = Kenalan.objects.filter(
        user_maba=user_maba,
        user_elemen__profile__batch__name=BATCH_NAME['2019']
    ).exclude(status=KenalanStatus.NOT_FRIEND.name).count()

    user_statistic.kenalan_created_quanta = Kenalan.objects.filter(
        user_maba=user_maba,
        user_elemen__profile__batch__name=BATCH_NAME['2018']
    ).exclude(status=KenalanStatus.NOT_FRIEND.name).count()

    user_statistic.kenalan_created_tarung = Kenalan.objects.filter(
        user_maba=user_maba,
        user_elemen__profile__batch__name=BATCH_NAME['2017']
    ).exclude(status=KenalanStatus.NOT_FRIEND.name).count()

    user_statistic.kenalan_created_elemen = Kenalan.objects.filter(
        user_maba=user_maba,
        user_elemen__profile__batch__name=BATCH_NAME_NON_ACTIVE
    ).exclude(status=KenalanStatus.NOT_FRIEND.name).count()

    user_statistic.kenalan_created_all = Kenalan.objects.filter(
        user_maba=user_maba
    ).exclude(status=KenalanStatus.NOT_FRIEND.name).count()

    user_statistic.kenalan_approved_maung = Kenalan.objects.filter(
        user_maba=user_maba,
        user_elemen__profile__batch__name=BATCH_NAME['2019'],
        status=KenalanStatus.APPROVED.name
    ).count()

    user_statistic.kenalan_approved_quanta = Kenalan.objects.filter(
        user_maba=user_maba,
        user_elemen__profile__batch__name=BATCH_NAME['2018'],
        status=KenalanStatus.APPROVED.name
    ).count()

    user_statistic.kenalan_approved_tarung = Kenalan.objects.filter(
        user_maba=user_maba,
        user_elemen__profile__batch__name=BATCH_NAME['2017'],
        status=KenalanStatus.APPROVED.name
    ).count()

    user_statistic.kenalan_approved_elemen = Kenalan.objects.filter(
        user_maba=user_maba,
        user_elemen__profile__batch__name=BATCH_NAME_NON_ACTIVE,
        status=KenalanStatus.APPROVED.name
    ).count()

    user_statistic.kenalan_approved_all = Kenalan.objects.filter(
        user_maba=user_maba,
        status=KenalanStatus.APPROVED.name
    ).count()

    user_statistic.save()
