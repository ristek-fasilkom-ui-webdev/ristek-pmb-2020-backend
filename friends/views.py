from django.contrib.auth.models import User
from django.db import transaction
from django.db.models import F, Q
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.utils.crypto import get_random_string
from rest_framework import permissions, status, generics
from rest_framework.response import Response
from rest_framework.views import APIView

from account.permissions import IsElemen, IsMaba, is_maba
from account.serializers import UserDetailSerializer

from friends.models import Kenalan, KenalanStatus, Token, KenalanTask, KenalanUserStatistics
from friends.serializers import (ElemenKenalanSerializer, GetKenalanSerializer,
                                 MabaKenalanSerializer, TokenSerializer,
                                 KenalanTaskSerializer, KenalanUserStatisticsSerializer)
from friends.utils import delete_expired_token


class TokenAPIView(APIView):
    permission_classes = [IsElemen]

    def get_token(self):
        token = get_random_string(length=6, allowed_chars='1234567890')

        if Token.objects.filter(token=token).exists():
            return self.get_token()
        return token

    def get(self, request):
        user = request.user
        count = request.GET.get('count', None)
        delete_expired_token(user)

        if Token.objects.filter(user=user).exists():
            token_obj = Token.objects.get(user=user)
            serialized_token = TokenSerializer(token_obj)
            return Response(serialized_token.data, status=status.HTTP_200_OK)
        elif count is not None:
            count = int(count)

            if count < 1 or count > 15:
                return Response(
                    data={'Detail': 'Count must be in range 1 to 15'},
                    status=status.HTTP_400_BAD_REQUEST
                )

            token = self.get_token()
            token_obj = Token.objects.create(token=token, user=user, count=count)
            serialized_token = TokenSerializer(token_obj)

            return Response(serialized_token.data, status=status.HTTP_201_CREATED)
        else:
            return Response(
                data={'Detail': 'Expired/used token, please regenerate'},
                status=status.HTTP_404_NOT_FOUND
            )

class KenalanListView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request):
        user = request.user

        if is_maba(user):
            kenalan_objects = Kenalan.objects.filter(user_maba=user)
        else:
            kenalan_objects = Kenalan.objects.filter(user_elemen=user).exclude(
                Q(status=KenalanStatus.NOT_FRIEND.name) | Q(status=KenalanStatus.SAVED.name)
            )

        serialized_kenalan = GetKenalanSerializer(kenalan_objects, many=True)
        return Response(serialized_kenalan.data)

    def post(self, request):
        permission_classes = [IsMaba]

        with transaction.atomic():
            token_obj = get_object_or_404(
                Token.objects.select_for_update(),
                token=request.data['token']
            )

            if token_obj.end_time < timezone.now():
                delete_expired_token(token_obj.user)
                return Response({'detail': 'Token Expired'}, status=status.HTTP_400_BAD_REQUEST)

            user_maba = request.user
            user_elemen = token_obj.user

            if Kenalan.objects.filter(user_maba=user_maba, user_elemen=user_elemen).exists():
                kenalan_obj = Kenalan.objects.get(user_maba=user_maba, user_elemen=user_elemen)
                serialized_kenalan = GetKenalanSerializer(kenalan_obj)

                return Response(serialized_kenalan.data, status=status.HTTP_200_OK)
            else:
                if token_obj.count <= 0:
                    return Response({'detail': 'Token used up'}, status=status.HTTP_400_BAD_REQUEST)
                token_obj.count = F('count') - 1
                token_obj.save()
                token_obj.refresh_from_db()

                kenalan_obj = Kenalan.objects.create(user_maba=user_maba, user_elemen=user_elemen)
                serialized_kenalan = GetKenalanSerializer(kenalan_obj)

                return Response(serialized_kenalan.data, status=status.HTTP_201_CREATED)

class KenalanDetailView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, pk):
        user = request.user

        kenalan_obj = get_object_or_404(Kenalan, id=pk)
        is_user_as_owner_maba = kenalan_obj.user_maba == user
        is_user_as_owner_elemen = kenalan_obj.user_elemen == user
        if is_user_as_owner_maba or is_user_as_owner_elemen:
            serialized_kenalan = GetKenalanSerializer(kenalan_obj)
            return Response(serialized_kenalan.data)
        return Response({'detail': 'Forbidden'}, status=status.HTTP_403_FORBIDDEN)

    def patch(self, request, pk):
        user = request.user

        kenalan_obj = get_object_or_404(Kenalan, id=pk)
        is_user_as_owner_maba = kenalan_obj.user_maba == user
        is_user_as_owner_elemen = kenalan_obj.user_elemen == user

        if is_user_as_owner_maba or is_user_as_owner_elemen:
            if is_user_as_owner_maba:
                serialized_data = MabaKenalanSerializer(
                    kenalan_obj,
                    data=request.data,
                    partial=True
                )
            else:
                serialized_data = ElemenKenalanSerializer(
                    kenalan_obj,
                    data=request.data,
                    partial=True
                )

            if serialized_data.is_valid():
                updated_kenalan_obj = serialized_data.save()
                serialized_updated_kenalan_obj = GetKenalanSerializer(updated_kenalan_obj)
                return Response(serialized_updated_kenalan_obj.data)
            return Response(serialized_data.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response({'detail': 'Forbidden'}, status=status.HTTP_403_FORBIDDEN)

    def delete(self, request, pk):
        user = request.user

        kenalan_obj = get_object_or_404(Kenalan, id=pk)
        is_user_as_owner_maba = kenalan_obj.user_maba == user
        if is_user_as_owner_maba:
            if kenalan_obj.status == KenalanStatus.APPROVED.name or \
                kenalan_obj.status == KenalanStatus.PENDING.name or \
                kenalan_obj.status == KenalanStatus.REJECTED.name:
                return Response({'detail': 'Forbidden'}, status=status.HTTP_403_FORBIDDEN)

            kenalan_obj.status = KenalanStatus.NOT_FRIEND.name
            kenalan_obj.description = ""
            kenalan_obj.rejection_message = ""

            kenalan_obj.save()

            return Response(
                {'detail': 'Successfully set to NOT_FRIEND'},
                status=status.HTTP_204_NO_CONTENT
            )
        return Response({'detail': 'Forbidden'}, status=status.HTTP_403_FORBIDDEN)

class KenalanTaskList(generics.ListAPIView):
    queryset = KenalanTask.objects.all()
    serializer_class = KenalanTaskSerializer
    permission_classes = [permissions.IsAuthenticated]

class KenalanUserStatisticsList(generics.ListAPIView):
    serializer_class = KenalanUserStatisticsSerializer
    permission_classes = [IsMaba]

    def get_queryset(self):
        return KenalanUserStatistics.objects.filter(user=self.request.user)

class FindFriend(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request):
        query = request.GET.get('query', None)

        if not query:
            return Response(
                data=[],
                status=status.HTTP_200_OK,
            )

        # Example: if query is 'cahya' then the variable result is '%cahya%'
        friend_name = '%%%s%%' % query
        interest_name = '%%%s%%' % query
        user = request.user
        potential_friends = User.objects.raw(
            """
                SELECT U.*, P.*, A.* FROM auth_user AS U
                LEFT JOIN account_userprofile AS P
                ON U.id = P.user_id
                LEFT JOIN account_batch AS A
                ON A.id = P.batch_id
                WHERE (
                    P.name ILIKE %s OR P.id IN (
                        SELECT P.id FROM account_userprofile AS P
                        LEFT JOIN account_profileinterest AS IP
                        ON IP.profile_id = P.id
                        LEFT JOIN account_interest AS I
                        ON IP.interest_id = I.id
                        WHERE I.interest_name ILIKE %s
                    )
                )
                AND P.is_private = FALSE
                AND U.id != %s
                ORDER BY RANDOM()
                LIMIT 30
            """, [friend_name, interest_name, user.id]
        )

        serialized_potential_friends = UserDetailSerializer(potential_friends, many=True)
        return Response(serialized_potential_friends.data, status=status.HTTP_200_OK)
