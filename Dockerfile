# Pull the base docker image
FROM python:3

# Set our app working directory
WORKDIR /pmb-backend

# Install psycopg2 dependencies
RUN apt-get -y install gcc libpq-dev

# Install dependencies
RUN pip install --upgrade pip
COPY ./requirements.txt ./
RUN pip install -r requirements.txt

# Copy project
COPY . ./