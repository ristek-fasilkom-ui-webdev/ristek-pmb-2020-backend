from django.contrib.auth.models import User
from rest_framework import serializers

from account.models import (Batch, BigGroup, Elemen, Group, Interest, Maba,
                            SmallGroup, UploadPhoto, UserProfile)
from account.utils import transform_profile_interests_list


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username',)
        read_only_fields = ('id', 'username',)

class BatchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Batch
        fields = '__all__'
        read_only_fields = ('year', 'name',)

class BigGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = BigGroup
        fields = '__all__'
        read_only_fields = ('name',)

class SmallGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = SmallGroup
        fields = '__all__'
        read_only_fields = ('name',)

class GroupSerializer(serializers.ModelSerializer):
    big_group = BigGroupSerializer()
    small_group = SmallGroupSerializer()

    class Meta:
        model = Group
        fields = '__all__'
        read_only_fields = ('big_group', 'small_group',)

class EmbeddedProfileInterestSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    interest_category = serializers.CharField()
    interest_name = serializers.CharField()
    is_top_interest = serializers.BooleanField()
    order = serializers.IntegerField()

    class Meta:
        read_only_fields = ['id', 'interest_category', 'interest_name', 'is_top_interest', 'order']

class UserProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    batch = BatchSerializer()
    interests = serializers.SerializerMethodField()

    class Meta:
        model = UserProfile
        fields = '__all__'
        read_only_fields = (
            'user',
            'name',
            'npm',
            'batch',
            'major',
            'email',
            'is_maba',
            'is_elemen',
            'created_at',
            'updated_at'
        )

    def get_interests(self, obj):
        profile_interests_dict_list = transform_profile_interests_list(obj.interests.all())

        serialized_interests = EmbeddedProfileInterestSerializer(
            profile_interests_dict_list,
            many=True
        )
        return serialized_interests.data

class MabaSerializer(serializers.ModelSerializer):
    profile = UserProfileSerializer()
    group = GroupSerializer()

    class Meta:
        model = Maba
        fields = '__all__'
        read_only_fields = ('created_at', 'updated_at',)

    def update(self, instance, validated_data):
        profile_data = validated_data.get('profile')
        if profile_data is not None:
            profile_serializer = UserProfileSerializer(
                instance.profile,
                data=profile_data,
                partial=True
            )
            if profile_serializer.is_valid():
                instance.profile = profile_serializer.save()

        group_data = validated_data.get('group')
        if group_data is not None:
            group_object = Group.objects.get(id=group_data['id'])
            instance.group = group_object
        instance.save()

        return instance

class ElemenSerializer(serializers.ModelSerializer):
    profile = UserProfileSerializer()

    class Meta:
        model = Elemen
        fields = '__all__'
        read_only_fields = ('created_at', 'updated_at',)

    def update(self, instance, validated_data):
        profile_data = validated_data.get('profile')
        if profile_data is not None:
            profile_serializer = UserProfileSerializer(
                instance.profile,
                data=profile_data,
                partial=True
            )
            if profile_serializer.is_valid():
                instance.profile = profile_serializer.save()

        instance.title = validated_data.get('title', instance.title)
        instance.save()

        return instance

class UserDetailSerializer(serializers.ModelSerializer):
    user_detail = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ['id', 'username', 'user_detail']
        read_only_fields = ['id', 'username', 'user_detail']

    def get_user_detail(self, obj):
        profile_obj = obj.profile

        if profile_obj.is_maba:
            serialized_obj = MabaSerializer(profile_obj.maba)
        else:
            serialized_obj = ElemenSerializer(profile_obj.elemen)
        
        return serialized_obj.data

class MorePrivateProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    batch = BatchSerializer()

    class Meta:
        model = UserProfile
        fields = ['user', 'name', 'about', 'batch', 'major', 'photo', 'created_at', 'updated_at']

class InterestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Interest
        fields = '__all__'

class CreateProfileInterestSerializer(serializers.Serializer):
    technology = serializers.ListField(child=serializers.CharField())
    non_tech = serializers.ListField(child=serializers.CharField())
    entertainment = serializers.ListField(child=serializers.CharField())

class UploadPhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = UploadPhoto
        fields = '__all__'
