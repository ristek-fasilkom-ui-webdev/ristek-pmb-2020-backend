from urllib import parse as urllib_parse

from django.conf import settings
from django.contrib.auth import logout as auth_logout
from django.http import (
    HttpRequest,
    HttpResponse,
    HttpResponseRedirect,
)
from django.shortcuts import render

from django_cas_ng import views as baseviews
from django_cas_ng.models import ProxyGrantingTicket, SessionTicket
from django_cas_ng.signals import cas_user_logout
from django_cas_ng.utils import (
    get_cas_client,
    get_protocol,
    get_redirect_url,
)

from rest_framework_jwt.settings import api_settings
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

class PMBLoginView(baseviews.LoginView):
    def successful_login(self, request: HttpRequest, next_page: str) -> HttpResponse:
        """
        This method is called on successful login.
        Overriden to render a page that send JWT token via postMessage
        if the page receive a message from one of the whitelisted origin.
        """
        user = request.user
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)

        data = {
            'token': token,
            'cors_origin_regex_whitelist': settings.CORS_ORIGIN_REGEX_WHITELIST,
            'next_page': next_page,
        }
        
        return render(request, 'data_to_frontend.html', data)

class PMBLogoutView(baseviews.LogoutView):
    def get(self, request: HttpRequest) -> HttpResponse:
        """
        Redirects to CAS logout page

        :param request:
        :return:
        """
        next_page = request.GET.get('next')

        # try to find the ticket matching current session for logout signal
        try:
            st = SessionTicket.objects.get(session_key=request.session.session_key)
            ticket = st.ticket
        except SessionTicket.DoesNotExist:
            ticket = None
        # send logout signal
        cas_user_logout.send(
            sender="manual",
            user=request.user,
            session=request.session,
            ticket=ticket,
        )

        # clean current session ProxyGrantingTicket and SessionTicket
        ProxyGrantingTicket.objects.filter(session_key=request.session.session_key).delete()
        SessionTicket.objects.filter(session_key=request.session.session_key).delete()
        auth_logout(request)

        next_page = next_page or get_redirect_url(request)
        if settings.CAS_LOGOUT_COMPLETELY:
            redirect_url = next_page
            if next_page is None or (not next_page.startswith("http://") and not next_page.startswith("https://")):
                protocol = get_protocol(request)
                host = request.get_host()
                redirect_url = urllib_parse.urlunparse(
                    (protocol, host, next_page, '', '', ''),
                )

            client = get_cas_client(request=request)
            return HttpResponseRedirect(client.get_logout_url(redirect_url))

        # This is in most cases pointless if not CAS_RENEW is set. The user will
        # simply be logged in again on next request requiring authorization.
        return HttpResponseRedirect(next_page)