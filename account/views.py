from django.contrib.auth.models import User
from django.db import IntegrityError, transaction
from django.shortcuts import get_object_or_404
from rest_framework import generics, status
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from account.models import Elemen, Group, Interest, Maba, UserProfile, ProfileInterest
from account.permissions import IsOwner, is_maba
from account.serializers import (ElemenSerializer, InterestSerializer,
                                 CreateProfileInterestSerializer, EmbeddedProfileInterestSerializer,
                                 GroupSerializer, MabaSerializer, UploadPhotoSerializer)
from account.utils import transform_profile_interests_list


class ProfileDetailAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, pk):
        user = get_object_or_404(User, id=pk)
        profile = get_object_or_404(UserProfile, user=user)

        if is_maba(user):
            maba = Maba.objects.get(profile=profile)
            serialized_maba = MabaSerializer(maba)
            return Response(serialized_maba.data)
        else:
            elemen = Elemen.objects.get(profile=profile)
            serialized_elemen = ElemenSerializer(elemen)
            return Response(serialized_elemen.data)

class MyProfileAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        profile = get_object_or_404(UserProfile, user=request.user)

        if is_maba(request.user):
            maba = Maba.objects.get(profile=profile)
            serialized_maba = MabaSerializer(maba)
            return Response(serialized_maba.data)
        else:
            elemen = Elemen.objects.get(profile=profile)
            serialized_elemen = ElemenSerializer(elemen)
            return Response(serialized_elemen.data)

    def patch(self, request):
        self.permission_classes = [IsOwner]
        profile = get_object_or_404(UserProfile, user=request.user)
        if is_maba(request.user):
            maba = get_object_or_404(Maba, profile=profile)
            serializer = MabaSerializer(maba, data=request.data, partial=True)
            serializer.is_valid(raise_exception=True)

            updated_profile = serializer.save()
            serialized_update_profile = MabaSerializer(updated_profile)
            return Response(serialized_update_profile.data)
        else:
            elemen = get_object_or_404(Elemen, profile=profile)
            serializer = ElemenSerializer(elemen, data=request.data, partial=True)
            serializer.is_valid(raise_exception=True)

            updated_profile = serializer.save()
            serialized_updated_profile = ElemenSerializer(updated_profile)
            return Response(serialized_updated_profile.data)

class GroupListView(generics.ListAPIView):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [IsAuthenticated]

class InterestListView(generics.ListAPIView):
    serializer_class = InterestSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Interest.objects.all()

class ProfileInterestListView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        profile = get_object_or_404(UserProfile, user=request.user)
        profile_interests_dict_list = transform_profile_interests_list(
            ProfileInterest.objects.filter(profile=profile).all()
        )

        serialized_interests = EmbeddedProfileInterestSerializer(
            profile_interests_dict_list,
            many=True
        )

        return Response(serialized_interests.data, status=status.HTTP_200_OK)

    def post(self, request):
        profile = get_object_or_404(UserProfile, user=request.user)

        interest_list = Interest.objects.all().values('id', 'interest_name', 'interest_category')
        interest_by_category_dict = {
            'Technology': [],
            'Non-Tech': [],
            'Entertainment': []
        }
        interest_and_id_mapping = {
            'Technology': {},
            'Non-Tech': {},
            'Entertainment': {}
        }

        for interest in interest_list:
            name = interest['interest_name']
            category = interest['interest_category']
            interest_by_category_dict[category].append(name)

            interest_and_id_mapping[category][name] = interest['id']

        serializer = CreateProfileInterestSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        key_renamed_validated_data = {}
        key_renamed_validated_data['Technology'] = serializer.validated_data.pop('technology')
        key_renamed_validated_data['Non-Tech'] = serializer.validated_data.pop('non_tech')
        key_renamed_validated_data['Entertainment'] = serializer.validated_data.pop('entertainment')

        try:
            with transaction.atomic():
                ProfileInterest.objects.filter(profile=profile).delete()

                profile_interest_obj = []
                for category, interest_name_list in key_renamed_validated_data.items():
                    order = 1
                    for interest_name in interest_name_list:
                        if interest_name not in interest_by_category_dict[category]:
                            created_interest = Interest(
                                interest_name=interest_name,
                                interest_category=category
                            )
                            created_interest.save()
                            interest_by_category_dict[category].append(interest_name)
                            interest_and_id_mapping[category][interest_name] = created_interest.id

                        profile_interest_obj.append(ProfileInterest(
                            profile=profile,
                            interest_id=interest_and_id_mapping[category][interest_name],
                            is_top_interest=True if order == 1 else False,
                            order=order
                        ))
                        order = order + 1

                ProfileInterest.objects.bulk_create(profile_interest_obj)

            profile_interests_dict_list = transform_profile_interests_list(
                ProfileInterest.objects.filter(profile=profile).all()
            )

            serialized_interests = EmbeddedProfileInterestSerializer(
                profile_interests_dict_list,
                many=True
            )

            return Response(serialized_interests.data, status=status.HTTP_201_CREATED)
        except IntegrityError as error:
            return Response(str(error), status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class UploadPhotoAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    parser_classes = (MultiPartParser, FormParser)

    def post(self, request):
        serializer = UploadPhotoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
