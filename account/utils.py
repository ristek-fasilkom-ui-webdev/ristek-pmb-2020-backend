PREFIX_YEAR = '20'

BATCH_NAME_NON_ACTIVE = 'Elemen'
BATCH_NAME = {
    '2017': 'Tarung',
    '2018': 'Quanta',
    '2019': 'Maung',
    '2020': 'Maba',
}


def generate_email_from_username(username):
    return username.strip() + '@ui.ac.id'

def get_batch_from_npm(npm):
    suffix = npm[:2]
    return PREFIX_YEAR + suffix

def get_batch_name_from_batch_year(batch_year):
    if batch_year in BATCH_NAME:
        return BATCH_NAME[batch_year]

    return BATCH_NAME_NON_ACTIVE

def transform_profile_interests_list(queryset):
    profile_interests_dict_list = []

    for profile_interest_obj in queryset:
        profile_interests_dict_list.append({
            'id': profile_interest_obj.id,
            'interest_category': profile_interest_obj.interest.interest_category,
            'interest_name': profile_interest_obj.interest.interest_name,
            'is_top_interest': profile_interest_obj.is_top_interest,
            'order': profile_interest_obj.order
        })

    return profile_interests_dict_list
