import json
import os

from django.conf import settings
from django.contrib.auth.models import Group as AuthGroup
from django.dispatch import receiver

from django_cas_ng.signals import cas_user_authenticated

from account.data.org import org_major_mapping
from account.models import Batch, UserProfile, Maba, Elemen, BigGroup, SmallGroup, Group
from account.utils import (
    generate_email_from_username,
    get_batch_from_npm,
    get_batch_name_from_batch_year,
)

@receiver(cas_user_authenticated)
def cas_create_user_profile_if_new_user(sender, user, created, attributes, **kwargs):
    if not UserProfile.objects.filter(user=user).exists():
        name = attributes['nama']
        npm = attributes['npm'] if 'npm' in attributes else attributes['nip']
        email = generate_email_from_username(user.get_username())

        batch_year = get_batch_from_npm(npm)
        batch_name = get_batch_name_from_batch_year(batch_year)
        batch, created = Batch.objects.get_or_create(
            year=batch_year,
            name=batch_name
        )

        major = ''
        if 'kd_org' in attributes and attributes['kd_org'] in org_major_mapping:
            major = org_major_mapping[attributes['kd_org']]

        profile = UserProfile.objects.create(
            user=user,
            name=name,
            npm=npm,
            batch=batch,
            major=major,
            email=email
        )

        if batch_year == '2020':
            big_group_str = ''
            small_group_str = ''
            big_group = None
            small_group = None
            group = None
            mentor = ''
            code = ''
            try:
                base_dir = settings.BASE_DIR
                """
                maba_data.json expected structure is a list of object maba:
                [
                    "<npm as string>": {
                        "big_group": "<big_group_name as string>",
                        "small_group": "<small_group_name as string>",
                        "mentor": "<mentor name as string>",
                        "code": "<code as string (usually kode absen)>"
                    }
                ]
                """
                maba_data_file_path = os.path.join(base_dir, 'static', 'data', 'maba_data.json')
                with open(maba_data_file_path) as maba_data_file:
                    maba_data = json.load(maba_data_file)
                    if npm in maba_data:
                        big_group_str = maba_data[npm]['big_group']
                        small_group_str = maba_data[npm]['small_group']
                        mentor = maba_data[npm]['mentor']
                        code = maba_data[npm]['code']
            except FileNotFoundError:
                pass

            if big_group_str != '' and small_group_str != '':
                big_group, created = BigGroup.objects.get_or_create(name=big_group_str)
                small_group, created = SmallGroup.objects.get_or_create(name=small_group_str)
                group, created = Group.objects.get_or_create(
                    big_group=big_group,
                    small_group=small_group
                )

            maba = Maba.objects.create(
                profile=profile,
                group=group,
                mentor=mentor,
                code=code
            )
            maba.profile.is_maba = True
            maba.profile.save()
        else:
            ### Debugging using title as title is not used in frontend anymore
            title = ''
            if attributes['peran_user'] == 'staff':
                title = json.dumps(attributes)

            elemen = Elemen.objects.create(
                profile=profile,
                title=title
            )
            elemen.profile.is_elemen = True
            elemen.profile.save()

            try:
                base_dir = settings.BASE_DIR
                """
                academic_staff.json expected structure is a list of string:
                [
                    "<npm as string>",
                    "<npm as string>"
                ]
                """
                academic_staff_data_file_path = os.path.join(base_dir, 'static', 'data', 'academic_staff_data.json')
                with open(academic_staff_data_file_path) as academic_staff_data_file:
                    academic_staff_data = json.load(academic_staff_data_file)
                    if npm in academic_staff_data:
                        auth_group, created = AuthGroup.objects.get_or_create(name="Academic Staff")
                        user.groups.add(auth_group)
                        user.is_staff = True
                        user.save()

                        create_maba_dummy = os.environ.get('CREATE_MABA_DUMMY_FOR_AKADEMIS', 'false')
                        if create_maba_dummy == 'true':
                            big_group, created = BigGroup.objects.get_or_create(name="Academic")
                            small_group, created = SmallGroup.objects.get_or_create(name="Staff")
                            group, created = Group.objects.get_or_create(
                                big_group=big_group,
                                small_group=small_group
                            )

                            maba = Maba.objects.create(
                                profile=profile,
                                group=group,
                            )
                            maba.profile.is_maba = True
                            maba.profile.save()   
            except FileNotFoundError:
                pass
