from django_cas_ng.backends import CASBackend

from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.contrib import messages

from account.data.org import allowed_org_list

class PMBCASBackend(CASBackend):
    def clean_username(self, username: str) -> str:
        """
        Performs any cleaning on the ``username`` prior to using it to get or
        create the user object.

        By default, changes the username case according to
        `settings.CAS_FORCE_CHANGE_USERNAME_CASE`.

        As SSO UI also allows trailing space in username, we strip the username.

        :param username: [string] username.

        :returns: [string] The cleaned username.
        """
        username_case = settings.CAS_FORCE_CHANGE_USERNAME_CASE
        if username_case == 'lower':
            username = username.lower()
        elif username_case == 'upper':
            username = username.upper()
        elif username_case is not None:
            raise ImproperlyConfigured(
                "Invalid value for the CAS_FORCE_CHANGE_USERNAME_CASE setting. "
                "Valid values are `'lower'`, `'upper'`, and `None`.")

        username = username.strip()

        return username

    def bad_attributes_reject(self, request, username, attributes):
        """
        Reject non-fasilkom from logging in to system. This only works if we are using CAS 2.
        If CAS 2 is down and we have to use CAS 3, students from other faculty can log in.
        """
        if 'kd_org' in attributes and attributes['kd_org'] not in allowed_org_list:
            message = 'User ' + \
                str(username) + \
                ' is not allowed to login because it is not part of Fakultas Ilmu Komputer.' + \
                'Please contact PMB if this is not true.'
            messages.add_message(request, messages.ERROR, message)
            return message

        return None
