from enum import Enum

from django.contrib.auth import get_user_model
from django.db import models


class Batch(models.Model):
    year = models.CharField(max_length=4)
    name = models.CharField(max_length=50)

    class Meta:
        ordering = ['year']
        verbose_name = 'Batch'
        verbose_name_plural = 'Batch'

    def __str__(self):
        return self.name

class BigGroup(models.Model):
    name = models.CharField(max_length=50, unique=True)

    class Meta:
        ordering = ['name']
        verbose_name = 'Big Group'
        verbose_name_plural = 'Big Groups'

    def __str__(self):
        return self.name

class SmallGroup(models.Model):
    name = models.CharField(max_length=50, unique=True)

    class Meta:
        ordering = ['name']
        verbose_name = 'Small Group'
        verbose_name_plural = 'Small Groups'

    def __str__(self):
        return self.name

class Group(models.Model):
    big_group = models.ForeignKey(BigGroup, on_delete=models.CASCADE)
    small_group = models.ForeignKey(SmallGroup, on_delete=models.CASCADE)

    class Meta:
        unique_together = ['big_group', 'small_group']
        ordering = ['big_group', 'small_group']
        verbose_name = 'Group'
        verbose_name_plural = 'Groups'

    def __str__(self):
        return str(self.big_group) + ' - ' + str(self.small_group)

class UserProfile(models.Model):
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE, related_name='profile')
    name = models.CharField(max_length=128, db_index=True)
    npm = models.CharField(max_length=10)
    batch = models.ForeignKey(Batch, on_delete=models.SET_NULL, null=True, db_index=True)
    major = models.CharField(max_length=50, blank=True)
    email = models.CharField(max_length=128)
    photo = models.URLField(blank=True)
    about = models.TextField(blank=True)
    high_school = models.CharField(max_length=100, blank=True)
    line_id = models.CharField(max_length=100, blank=True)
    phone_number = models.CharField(max_length=20, blank=True)
    website = models.URLField(max_length=100, blank=True)
    birth_place = models.CharField(max_length=100, blank=True)
    birth_date = models.DateField(auto_now=False, auto_now_add=False, blank=True, null=True)
    is_maba = models.BooleanField(default=False)
    is_elemen = models.BooleanField(default=False)
    is_private = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name

class Maba(models.Model):
    profile = models.OneToOneField(UserProfile, on_delete=models.CASCADE, related_name="maba")
    group = models.ForeignKey(Group, on_delete=models.SET_NULL, null=True, db_index=True)
    evaluator = models.CharField(max_length=128, blank=True)
    mentor = models.CharField(max_length=128, blank=True)
    code = models.CharField(max_length=128, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['profile__name']
        verbose_name = 'Maba'
        verbose_name_plural = 'Maba'

    def __str__(self):
        return str(self.profile)

class Elemen(models.Model):
    profile = models.OneToOneField(UserProfile, on_delete=models.CASCADE, related_name="elemen")
    title = models.CharField(max_length=128, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['profile__name']
        verbose_name = 'Elemen'
        verbose_name_plural = 'Elemen'

    def __str__(self):
        return str(self.profile)

class InterestCategory(Enum):
    TECHNOLOGY = 'Technology'
    NON_TECH = 'Non-Tech'
    ENTERTAINMENT = 'Entertainment'

    @staticmethod
    def get_default():
        return InterestCategory.TECHNOLOGY.value

class Interest(models.Model):
    interest_name = models.CharField(max_length=255)
    interest_category = models.CharField(
        max_length=255,
        choices=[(category.value, category.value) for category in InterestCategory],
        default=InterestCategory.get_default(),
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['interest_category', 'interest_name']

    def __str__(self):
        return self.interest_category + ' - ' + self.interest_name

class ProfileInterest(models.Model):
    profile = models.ForeignKey(UserProfile, related_name="interests", on_delete=models.CASCADE)
    interest = models.ForeignKey(Interest, related_name="profiles", on_delete=models.CASCADE)
    is_top_interest = models.BooleanField(default=False)
    order = models.IntegerField()

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['profile__name', 'interest__interest_category', 'order']

    def __str__(self):
        return self.profile.name + ' is interested in ' + self.interest.interest_name

class UploadPhoto(models.Model):
    photo = models.ImageField(upload_to='images/')

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['created_at']

    def __str__(self):
        return str(self.photo)
        