from django.urls import re_path
from django_cas_ng.views import CallbackView
from account.cas_views import PMBLoginView, PMBLogoutView
from account.views import (
    ProfileDetailAPIView,
    MyProfileAPIView,
    GroupListView,
    InterestListView,
    ProfileInterestListView,
    UploadPhotoAPIView
)

urlpatterns = [
    re_path(r'^login$', PMBLoginView.as_view(), name='cas_ng_login'),
    re_path(r'^logout$', PMBLogoutView.as_view(), name='cas_ng_logout'),
    re_path(r'^callback$', CallbackView.as_view(), name='cas_ng_proxy_callback'),
    re_path(r'^profile/(?P<pk>[0-9]+)$', ProfileDetailAPIView.as_view(), name='profile_detail'),
    re_path(r'^profile/me$', MyProfileAPIView.as_view(), name='my_profile'),
    re_path(r'^group$', GroupListView.as_view(), name='group_list'),
    re_path(r'^interests$', InterestListView.as_view(), name='interest_list'),
    re_path(r'^interests/me$', ProfileInterestListView.as_view(), name='my_interests'),
    re_path(r'^upload-photo$', UploadPhotoAPIView.as_view(), name='upload_photo'),
]
