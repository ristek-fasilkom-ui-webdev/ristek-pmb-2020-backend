allowed_org_list = [
    '01.00.12.01',
    '02.00.12.01',
    '03.00.12.01',
    '04.00.12.01',
    '05.00.12.01',
    '06.00.12.01',
    '07.00.12.01',
    '08.00.12.01',
    '09.00.12.01'
]

org_major_mapping = {
    '01.00.12.01': 'Ilmu Komputer',
    '02.00.12.01': 'Ilmu Komputer',
    '06.00.12.01': 'Sistem Informasi',
    '08.00.12.01': 'Sistem Informasi',
    '09.00.12.01': 'Ilmu Komputer'
}
