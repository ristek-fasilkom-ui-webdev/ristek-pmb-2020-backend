from django.contrib import admin

from account.models import (Batch, BigGroup, Elemen, Group, Interest, Maba,
                            SmallGroup, UploadPhoto, UserProfile, ProfileInterest)


class BatchModelAdmin(admin.ModelAdmin):
    list_display = ['name', 'year']
    search_fields = ['name', 'year']

    class Meta:
        model = Batch

class UserProfileModelAdmin(admin.ModelAdmin):
    list_display = ['name', 'npm', 'batch', 'user']
    search_fields = ['name', 'npm', 'user__username']
    list_filter = ['batch']

    class Meta:
        model = UserProfile

class GroupModelAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'big_group', 'small_group']
    list_filter = ['big_group']

    class Meta:
        model = Group

class MabaModelAdmin(admin.ModelAdmin):
    list_display = ['profile', 'group']
    search_fields = ['profile__name', 'group__big_group__name', 'group__small_group__name']
    list_filter = ['group']

    class Meta:
        model = Maba

class ElemenModelAdmin(admin.ModelAdmin):
    list_display = ['profile', 'title']
    search_fields = ['profile__name']

    class Meta:
        model = Elemen

class UploadPhotoModelAdmin(admin.ModelAdmin):
    class Meta:
        model = UploadPhoto

class InterestModelAdmin(admin.ModelAdmin):
    search_fields = ['interest_name', 'interest_category']
    list_filter = ['interest_category']

    class Meta:
        model = Interest

class ProfileInterestModelAdmin(admin.ModelAdmin):
    search_fields = ['profile__name', 'interest__interest_name', 'interest__interest_category']
    list_filter = ['interest__interest_name']

    class Meta:
        model = ProfileInterest

admin.site.register(Batch, BatchModelAdmin)
admin.site.register(UserProfile, UserProfileModelAdmin)
admin.site.register(BigGroup)
admin.site.register(SmallGroup)
admin.site.register(Group, GroupModelAdmin)
admin.site.register(Maba, MabaModelAdmin)
admin.site.register(Elemen, ElemenModelAdmin)
admin.site.register(Interest, InterestModelAdmin)
admin.site.register(ProfileInterest, ProfileInterestModelAdmin)
admin.site.register(UploadPhoto, UploadPhotoModelAdmin)
