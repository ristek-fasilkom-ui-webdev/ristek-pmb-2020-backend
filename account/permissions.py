from rest_framework import permissions
from .models import UserProfile

def is_elemen(user):
    profile = UserProfile.objects.filter(user=user)
    return profile[0].is_elemen

def is_maba(user):
    profile = UserProfile.objects.filter(user=user)
    return profile[0].is_maba

class IsElemen(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return is_elemen(request.user)

    def has_permission(self, request, view):
        return is_elemen(request.user)

class IsMaba(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return is_maba(request.user)

    def has_permission(self, request, view):
        return is_maba(request.user)

class IsOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        try:
            if hasattr(obj, 'user'):
                return obj.user == request.user
            elif hasattr(obj, 'author_id'):
                return obj.author_id == request.user.id
        except Exception as error:
            raise error
