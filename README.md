# RISTEK PMB 2020 Backend

This repository contain codes of Backend for PMB 2020 Web.

All instruction in this README is made with `Linux` in mind. Some commands may be different for `Windows` or `Mac`.

## Prerequisite
- Make sure you have `Python 3` and `pip` installed on your computer.
- `Linux`, `WSL`, or `Mac` is recommended, thought `Windows` should do fine.
- Read [this](https://www.psycopg.org/docs/install.html#install-from-source) and make sure the prerequisite listed is installed. For `Windows`, search for `how to install psycopg2 windows`.
- Soft Prerequisite
    - Install [`docker`](https://www.docker.com/get-started). Not necessary for `DEV`, but certainly helpful, and absolutely necessary for deployment.

## How to run (Non-Docker)
- Create a virtual environment (`python3 -m venv <virtual environment folder name>`).
- Activate the virtual environment (`source <virtual environment folder name>/bin/activate`).
- Install the dependencies (`pip install -r requirements.txt`).
- Copy the environment file (`.env-defaults`) and rename the copy to `.env`. Modify as necessary.
- Make migrations and migrate (`python manage.py makemigrations && python manage.py migrate`).
- Run the app (`python manage.py runserver`). By default, the app should run on port `8000`.

## How to run (Docker)
- Copy the `docker-compose.dev.yml` (or `docker-compose.staging.yml` or `docker-compose.production.yml`, according to desired environment) file from `deployment` folder to your root project folder (the folder containing `Dockerfile`).
- Copy the environment file (`.env-defaults`) and rename the copy to `.env`. Modify as necessary.
- Run `docker-compose -f docker-compose.dev.yml up --build` (or `docker-compose -f docker-compose.staging.yml up --build` for STAGING, or `docker-compose -f docker-compose.production.yml up --build` for PRODUCTION).
- Make migrations and migrate (`docker-compose exec web-<ENV> python manage.py makemigrations` and `docker-compose exec web-<ENV> python manage.py migrate`, example for DEV: `docker-compose exec web-DEV python manage.py makemigrations`)
- **Note**: DEV compose should run correctly without further configuration.   
However, STAGING and PRODUCTION compose require more configuration that will be detailed [here](#intended-docker-architecture). You can make your own architecture too without relying on STAGING and PRODUCTION compose.

## Environment Variables
Here are the list of available environment variables:

```
ENV - Denote which environment the app will be run in. Valid values are DEV, STAGING, and PRODUCTION. The differences are:
- DEV use SQLite, STAGING and PRODUCTION use PostgreSQL.
- DEV and STAGING use DEBUG.

SECRET_KEY - Django Secret Key.
JWT_SECRET_KEY - Secret Key used by JWT to decode and encode.

ALLOWED_HOSTS - A string of hosts separated by space. Represent the host/domain names that this Django site can serve.
CORS_ORIGIN_REGEX_WHITELIST - A string of hosts (regex supported) separated by space that are allowed to do cross-site requests with our app.

WEB_PORT - The port where our app runs.

# Postgres only configuration
DB_NAME - Name of the database schema.
DB_USER - User name of the database.
DB_PASSWORD - Password for the aforementioned user.
DB_HOST - Host of the database. In docker corresponded to the database container.
DB_PORT - Port of the database.

# Architecture related configuration
HOST_STATIC_PATH - Folder on server/host that will be used by docker as static volume
HOST_MEDIA_PATH - Folder on server/host that will be used by docker as media volume
NGINX_NETWORK - Name of the docker network used by NGINX
```

## Development Guidelines
- English names if possible.
- Empty line at the end of file.
- After installing a package, don't forget to `pip freeze` and check for `pkg-resources==0.0.0`. See [appendix](##Appendix).
- Follow **PEP-8** style guidelines. `py-lint` is already included in `requirements.txt`, just make sure your IDE can load it.
- Use `isort` to reorder your import according to PEP-8 style guidelines. E.g. `isort account/views.py`.
- TODO

## Appendix

### Misc Notes
- Everytime you update `requirements.txt`, watch out for `pkg-resources==0.0.0`. Apparently this package should not be included, but `pip freeze` include it anyway due to a bug in **Ubuntu**. You should always delete it before commiting.

### Intended Docker Architecture
Due to the fact that this is a low budget, volunteer work, we are limited in terms of server capability. Also, our understanding of Docker is still limited. Any kind of suggestion or improvement is welcomed.

First, both STAGING and PRODUCTION are intended to be placed in one server. For this reason, the `web` and `db` service are differentiated by suffix `-STAGING` and `-PRODUCTION` on the compose file.

Second, we are using `nginx` as proxy server, and this `nginx` is deployed on a separate docker container, thus requiring us to connect STAGING, PRODUCTION, and `nginx` container via one shared docker network. This is where the environment variable `NGINX_NETWORK` used, where the variable denote the name of the docker network.

Third, due to our limited understanding of docker volume, we decided to place the volume that holds static and media files of `web` service on the host itself. This is where the environment variable `HOST_STATIC_PATH` and `HOST_MEDIA_PATH` comes into play, where both are used to tell docker compose where to place static and media files on host.

Configure all three correctly, and the STAGING and PRODUCTION compose can be easily used.